include ${FSLCONFDIR}/default.mk

PROJNAME = newmesh
SOFILES  = libfsl-newmeshclass.so
LIBS     = -lfsl-miscmaths -lfsl-utils -lfsl-giftiio
OBJS     = giftiInterface.o point.o newmesh.o Relations.o \
           resampler.o meshfns.o featurespace.o

all: libfsl-newmeshclass.so

libfsl-newmeshclass.so: ${OBJS}
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}
